/* Python module for OpenSync
 * Copyright (C) 2005  Eduardo Pereira Habkost <ehabkost@conectiva.com.br>
 * Copyright (C) 2007  Andrew Baumann <andrewb@cse.unsw.edu.au>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * @author Eduardo Pereira Habkost <ehabkost@conectiva.com.br>
 * @author Andrew Baumann <andrewb@cse.unsw.edu.au>
 *
 * Additional changes by Armin Bauer <armin.bauer@desscon.com>
 */

#include <Python.h>
#include <opensync/opensync.h>
#include <opensync/opensync-plugin.h>
#include <opensync/opensync-client.h>
#include <signal.h>
#include <glib.h>

/* change this define for python exception output on stderr */
//#define PYERR_CLEAR() PyErr_Clear()
#define PYERR_CLEAR() PyErr_Print()

typedef struct MemberData {
	PyObject *osync_module;
	PyObject *module;
	PyObject *init_return;
} MemberData;

static PyObject *pm_load_opensync(OSyncError **error)
{
	PyObject *osync_module = PyImport_ImportModule("opensync1");
	if (!osync_module) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldn't load OpenSync1 module");
		PYERR_CLEAR();
		return NULL;
	}
	return osync_module;
}

static PyObject *pm_make_change(PyObject *osync_module, OSyncChange *change, OSyncError **error)
{
	PyObject *pychg_cobject = PyCObject_FromVoidPtr(change, NULL);
	if (!pychg_cobject) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldnt make pychg cobject");
		PYERR_CLEAR();
		return NULL;
	}
	
	PyObject *pychg = PyObject_CallMethod(osync_module, "Change", "O", pychg_cobject);
	Py_DECREF(pychg_cobject);
	if (!pychg) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Cannot create Python OSyncChange");
		PYERR_CLEAR();
		return NULL;
	}
	return pychg;
}

static PyObject *pm_make_context(PyObject *osync_module, OSyncContext *ctx, OSyncError **error)
{
	PyObject *pyctx_cobject = PyCObject_FromVoidPtr(ctx, NULL);
	if (!pyctx_cobject) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldnt make pyctx cobject");
		PYERR_CLEAR();
		return NULL;
	}
	
	PyObject *pyctx = PyObject_CallMethod(osync_module, "Context", "O", pyctx_cobject);
	Py_DECREF(pyctx_cobject);
	if (!pyctx) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Cannot create Python OSyncContext");
		PYERR_CLEAR();
		return NULL;
	}
	return pyctx;
}

static PyObject *pm_make_info(PyObject *osync_module, OSyncPluginInfo *info, OSyncError **error)
{
	PyObject *pyinfo_cobject = PyCObject_FromVoidPtr(info, NULL);
	if (!pyinfo_cobject) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldnt make pyinfo cobject");
		PYERR_CLEAR();
		return NULL;
	}
	
	PyObject *pyinfo = PyObject_CallMethod(osync_module, "PluginInfo", "O", pyinfo_cobject);
	Py_DECREF(pyinfo_cobject);
	if (!pyinfo) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Cannot create Python OSyncPluginInfo");
		PYERR_CLEAR();
		return NULL;
	}
	return pyinfo;
}

/* convert a python exception to an OSyncError containing the traceback of the exception */
static void pm_pyexcept_to_oserror(PyObject *pytype, PyObject *pyvalue, PyObject *pytraceback, OSyncError **error)
{
	const char *errmsg = NULL;
	PyObject *tracebackmod = NULL, *stringmod = NULL;
	PyObject *pystrs = NULL, *pystr = NULL;
	
	tracebackmod = PyImport_ImportModule("traceback");
	if (!tracebackmod) {
		errmsg = "import traceback";
		goto error;
	}

	if (pytraceback == NULL) {
		pystrs = PyObject_CallMethod(tracebackmod, "format_exception_only", "OO", pytype, pyvalue);
		if (!pystrs) {
			errmsg = "traceback.format_exception_only";
			goto error;
		}
	} else {
		pystrs = PyObject_CallMethod(tracebackmod, "format_exception", "OOO", pytype, pyvalue, pytraceback);
		if (!pystrs) {
			errmsg = "traceback.format_exception";
			goto error;
		}
	}

	stringmod = PyImport_ImportModule("string");
	if (!stringmod) {
		errmsg = "import string";
		goto error;
	}

	pystr = PyObject_CallMethod(stringmod, "join", "Os", pystrs, "");
	if (!pystr) {
		errmsg = "string.join";
		goto error;
	}

	osync_error_set(error, OSYNC_ERROR_GENERIC, "%s", PyString_AsString(pystr));

error:
	Py_XDECREF(tracebackmod);
	Py_XDECREF(stringmod);
	Py_XDECREF(pystrs);
	Py_XDECREF(pystr);

	if (errmsg) {
		PYERR_CLEAR();
		osync_error_set(error, OSYNC_ERROR_GENERIC, "pm_pyexcept_to_oserror: failed to report error: exception in %s", errmsg);
	}
}

/** Call a python method, report any exception it raises as an error, if no exception was raised report success
 *
 * Methods called using this function can
 * have one of these formats:
 *
 * - function(info, context)
 * - function(info, context, change)
 */
static osync_bool pm_call_module_method(OSyncObjTypeSink *sink,
	OSyncPluginInfo *info, OSyncContext *ctx, osync_bool *pslow_sync,
	void *userdata, char *name, OSyncChange *chg)
{
	osync_trace(TRACE_ENTRY, "%s(%s, %p, %p, %p)", __func__, name, info, ctx, chg);
	PyObject *ret = NULL;
	OSyncError *error = NULL;
	osync_bool report_error = TRUE;
	PyObject *osync_module = NULL;

	PyGILState_STATE pystate = PyGILState_Ensure();

	PyObject *sink_pyobject = userdata;
	if (!sink_pyobject) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "%s: %s: sink has no callback object", __func__, name);
		goto error;
	}

	if (!(osync_module = pm_load_opensync(&error)))
		goto error;

	PyObject *pyinfo = pm_make_info(osync_module, info, &error);
	if (!pyinfo)
		goto error;

	PyObject *pycontext = pm_make_context(osync_module, ctx, &error);
	if (!pycontext) {
		Py_DECREF(pyinfo);
		goto error;
	}

	if (chg) {
		PyObject *pychange = pm_make_change(osync_module, chg, &error);
		if (!pychange) {
			Py_DECREF(pycontext);
			Py_DECREF(pyinfo);
			goto error;
		}
		
		ret = PyObject_CallMethod(sink_pyobject, name, "OOO", pyinfo, pycontext, pychange);
		
		Py_DECREF(pychange);
	}
	else if (pslow_sync) {
		// this is get_changes or connect_done, which passes a slow_sync flag
		ret = PyObject_CallMethod(sink_pyobject, name, "OOO",
			pyinfo, pycontext, (*pslow_sync ? Py_True : Py_False));
	}
	else {
		ret = PyObject_CallMethod(sink_pyobject, name, "OO", pyinfo, pycontext);
	}

	Py_DECREF(pyinfo);

	if (ret) {
		Py_DECREF(pycontext);
		Py_DECREF(ret);
		Py_XDECREF(osync_module);
		PyGILState_Release(pystate);
		osync_context_report_success(ctx);
		osync_trace(TRACE_EXIT, "%s", __func__);
		return TRUE;
	}

	/* an exception occurred. get the python exception data */
	PyObject *pytype, *pyvalue, *pytraceback;
	PyErr_Fetch(&pytype, &pyvalue, &pytraceback);
	
	PyObject *osyncerror = NULL;
	osyncerror = PyObject_GetAttrString(osync_module, "Error");
	if (!osyncerror) {
		PYERR_CLEAR();
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get OSyncError class object");
		goto out;
	}
	
	if (PyErr_GivenExceptionMatches(pytype, osyncerror)) {
		/* if it's an OSyncError, just report that up on the context object */
		PyObject *obj = PyObject_CallMethod(pyvalue, "report", "O", pycontext);
		if (!obj) {
			PYERR_CLEAR();
			osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed reporting OSyncError");
			goto out;
		}
		
		Py_DECREF(obj);
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Reported OSyncError");
		report_error = FALSE;
	} else if (PyErr_GivenExceptionMatches(pytype, PyExc_IOError)
	           || PyErr_GivenExceptionMatches(pytype, PyExc_OSError)) {
		/* for IOError or OSError, we just report the &error message */
		PyObject *pystr = PyObject_Str(pyvalue);
		if (!pystr) {
			PYERR_CLEAR();
			osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed reporting IOError/OSError");
			goto out;
		}

		osync_error_set(&error, OSYNC_ERROR_IO_ERROR, "%s", PyString_AsString(pystr));
		Py_DECREF(pystr);
	} else {
		/* for other exceptions, we report a full traceback */
		pm_pyexcept_to_oserror(pytype, pyvalue, pytraceback, &error);
	}

out:
	Py_DECREF(pycontext);
	Py_XDECREF(pytype);
	Py_XDECREF(pyvalue);
	Py_XDECREF(pytraceback);
	Py_XDECREF(osyncerror);

error:
	Py_XDECREF(osync_module);
	PyGILState_Release(pystate);
	if (report_error)
		osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	return FALSE;
}

static void pm_connect(OSyncObjTypeSink *sink, OSyncPluginInfo *info,
			OSyncContext *ctx, void *userdata)
{
	pm_call_module_method(sink, info, ctx, NULL, userdata, "connect", NULL);
}

static void pm_disconnect(OSyncObjTypeSink *sink, OSyncPluginInfo *info,
			OSyncContext *ctx, void *userdata)
{
	pm_call_module_method(sink, info, ctx, NULL, userdata, "disconnect", NULL);
}

static void pm_get_changes(OSyncObjTypeSink *sink, OSyncPluginInfo *info,
			OSyncContext *ctx, osync_bool slow_sync, void *userdata)
{
	pm_call_module_method(sink, info, ctx, &slow_sync, userdata,
		"get_changes", NULL);
}

static void pm_commit(OSyncObjTypeSink *sink, OSyncPluginInfo *info,
			OSyncContext *ctx, OSyncChange *change, void *userdata)
{	
	pm_call_module_method(sink, info, ctx, NULL, userdata, "commit", change);
}

static void pm_committed_all(OSyncObjTypeSink *sink, OSyncPluginInfo *info,
			OSyncContext *ctx, void *userdata)
{	
	pm_call_module_method(sink, info, ctx, NULL, userdata, "committed_all", NULL);
}

static void pm_read(OSyncObjTypeSink *sink, OSyncPluginInfo *info,
			OSyncContext *ctx, OSyncChange *change, void *userdata)
{	
	pm_call_module_method(sink, info, ctx, NULL, userdata, "read", change);
}

static void pm_sync_done(OSyncObjTypeSink *sink, OSyncPluginInfo *info,
			OSyncContext *ctx, void *userdata)
{
	pm_call_module_method(sink, info, ctx, NULL, userdata, "sync_done", NULL);
}

static void pm_connect_done(OSyncObjTypeSink *sink, OSyncPluginInfo *info,
			OSyncContext *ctx, osync_bool slow_sync, void *userdata)
{
	pm_call_module_method(sink, info, ctx, &slow_sync, userdata, "connect_done", NULL);
}


/** Calls the method initialize function
 *
 * The python initialize() function register one or more sink objects
 * that have the other plugin methods (get_changeinfo, commit, etc.)
 */
static void *pm_initialize(OSyncPlugin *plugin, OSyncPluginInfo *info, OSyncError **error)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p)", __func__, plugin, info, error);
	MemberData *data = g_malloc0(sizeof(MemberData));
	char *modulename = NULL;
	OSyncList *s, *sinks = NULL;
	OSyncObjTypeSink *sink = NULL;

	PyGILState_STATE pystate = PyGILState_Ensure();

	if (!data) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to allocate module data");
		goto error;
	}

	// The modulename is set in the xml file, so we can use it
	// here, but we only need it for initialize, so free it below
	// once we're done with it.  We set the plugin data to NULL here
	// as well.
	if (!(modulename = osync_plugin_get_data(plugin))) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to retrieve module name");
		goto error;
	}
	osync_plugin_set_data(plugin, NULL);

	if (!(data->osync_module = pm_load_opensync(error)))
		goto error;

	if (!(data->module = PyImport_ImportModule(modulename))) {
		PYERR_CLEAR();
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldn't load module %s", modulename);
		free(modulename);
		goto error;
	}
	free(modulename);

	PyObject *pyinfo = pm_make_info(data->osync_module, info, error);
	if (!pyinfo)
		goto error;

	PyObject *ret = PyObject_CallMethod(data->module, "initialize", "O", pyinfo);
	Py_DECREF(pyinfo);
	if (!ret) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldn't initialize module");
		PYERR_CLEAR();
		goto error;
	}

	/* loop through all objtype sinks, set up function pointers */
	sinks = osync_plugin_info_get_objtype_sinks(info);
	for (s = sinks; s; s = s->next) {
		sink = (OSyncObjTypeSink *)s->data;

		osync_objtype_sink_set_connect_func(sink, pm_connect);
		osync_objtype_sink_set_disconnect_func(sink, pm_disconnect);
		osync_objtype_sink_set_get_changes_func(sink, pm_get_changes);
		osync_objtype_sink_set_commit_func(sink, pm_commit);
		osync_objtype_sink_set_committed_all_func(sink, pm_committed_all);
		osync_objtype_sink_set_read_func(sink, pm_read);
                osync_objtype_sink_set_sync_done_func(sink, pm_sync_done);
                osync_objtype_sink_set_connect_done_func(sink, pm_connect_done);
	}
	osync_list_free(sinks);

	/* set functions for the main sink, if one is provided */
	sink = osync_plugin_info_get_main_sink(info);
	if (sink) {
		osync_objtype_sink_set_connect_func(sink, pm_connect);
		osync_objtype_sink_set_disconnect_func(sink, pm_disconnect);
		osync_objtype_sink_set_get_changes_func(sink, pm_get_changes);
		osync_objtype_sink_set_commit_func(sink, pm_commit);
		osync_objtype_sink_set_committed_all_func(sink, pm_committed_all);
		osync_objtype_sink_set_read_func(sink, pm_read);
		osync_objtype_sink_set_sync_done_func(sink, pm_sync_done);
		osync_objtype_sink_set_connect_done_func(sink, pm_connect_done);
	}

	/* return value can be module data, if it's not None, store it */
	if (ret == Py_None) {
	  Py_DECREF(ret);	// decrement for the CallMethod above
	  ret = NULL;
	}
	data->init_return = ret; /* if it's an object, this takes our ref to it */

	PyGILState_Release(pystate);
	osync_trace(TRACE_EXIT, "%s", __func__);
	return data;

error:
	Py_XDECREF(data->module);
	Py_XDECREF(data->osync_module);
	PyGILState_Release(pystate);
	if (data) free(data);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return NULL;
}

static osync_bool pm_discover(OSyncPluginInfo *info, void *data_in, OSyncError **error)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p)", __func__, data_in, info, error);

	MemberData *data = data_in;

	PyGILState_STATE pystate = PyGILState_Ensure();

	PyObject *pyinfo = pm_make_info(data->osync_module, info, error);
	if (!pyinfo)
		goto error;

	PyObject *ret = NULL;
	if (data->init_return) {
		ret = PyObject_CallMethod(data->module, "discover", "OO", pyinfo, data->init_return);
	} else {
		Py_INCREF(Py_None);
		ret = PyObject_CallMethod(data->module, "discover", "OO", pyinfo, Py_None);
		Py_DECREF(Py_None);
	}

	Py_DECREF(pyinfo);
	if (!ret)
		goto error;

	Py_DECREF(ret);
	PyGILState_Release(pystate);
	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;

error:
	osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldn't call discover method");
	PYERR_CLEAR();
	PyGILState_Release(pystate);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}

static void pm_finalize(void *data)
{
	osync_trace(TRACE_ENTRY, "%s(%p)", __func__, data);
	MemberData *mydata = data;
	OSyncError **error = NULL;
	PyGILState_STATE pystate = PyGILState_Ensure();
	PyObject *ret = NULL;
	int has_finalize = 0;

	/* we make finalize optional in the plugin */
	if ((has_finalize = PyObject_HasAttrString(mydata->module, "finalize")) == 1) {
		if (mydata->init_return) {
			ret = PyObject_CallMethod(mydata->module, "finalize", "O", mydata->init_return);
		} else {
			Py_INCREF(Py_None);
			ret = PyObject_CallMethod(mydata->module, "finalize", "O", Py_None);
			Py_DECREF(Py_None);
		}
	}

	if (mydata->init_return) Py_DECREF(mydata->init_return);
	Py_DECREF(mydata->module);
	Py_DECREF(mydata->osync_module);
	free(mydata);

	if ((has_finalize == 1) && (!ret))
		goto error;

	Py_XDECREF(ret);
	PyGILState_Release(pystate);
	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

error:
	osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldn't call finalize method");
	PYERR_CLEAR();
	PyGILState_Release(pystate);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return;
}

/* set python search path to look in our module directory first */
static osync_bool set_search_path(OSyncError **error)
{
	PyObject *sys_module = PyImport_ImportModule("sys");
	if (!sys_module) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Couldn't import sys module");
		PYERR_CLEAR();
		return FALSE;
	}

	PyObject *path = PyObject_GetAttrString(sys_module, "path");
	if (!path) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "sys module has no path attribute?");
		PYERR_CLEAR();
		Py_DECREF(sys_module);
		return FALSE;
	}

	if (!PyList_Check(path)) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "sys.path is not a list?");
		Py_DECREF(path);
		Py_DECREF(sys_module);
		return FALSE;
	}

	PyObject *plugindir = Py_BuildValue("s", OPENSYNC_PYTHONPLG_DIR);
	if (!plugindir) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Error constructing plugindir string for sys.path");
		PYERR_CLEAR();
		Py_DECREF(path);
		Py_DECREF(sys_module);
		return FALSE;
	}

	int r = PySequence_Contains(path, plugindir);
	if (r < 0) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Error checking for 'plugindir in sys.path'");
		PYERR_CLEAR();
		Py_DECREF(plugindir);
		Py_DECREF(path);
		Py_DECREF(sys_module);
		return FALSE;
	}

	if (r == 0 && PyList_Insert(path, 0, plugindir) != 0) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Error inserting plugin directory into sys.path");
		PYERR_CLEAR();
		Py_DECREF(plugindir);
		Py_DECREF(path);
		Py_DECREF(sys_module);
		return FALSE;
	}

	Py_DECREF(plugindir);
	Py_DECREF(path);
	Py_DECREF(sys_module);

	return TRUE;
}

int main(int argc, char *argv[])
{
	osync_trace(TRACE_ENTRY, "%s(%d, %p)", __func__, argc, argv);

	const char *modulename = NULL;
	const char *pipe_path = NULL;
	int arg = 1;
	OSyncError *error = NULL;
	OSyncClient *client = NULL;
	OSyncPlugin *plugin = NULL;

	//
	// command line args
	//

	// check for plugin modulename
	if (argc >= (arg+1)) {
		modulename = argv[arg];
		arg++;
	}
	else {
		fprintf(stderr, "module name is missing!\n");
		return 1;
	}

	// check for pipe path
	if (argc >= (arg+1)) {
		pipe_path = argv[arg];
		arg++;
	}
	else {
		fprintf(stderr, "pipe path is missing!\n");
		return 2;
	}


	//
	// load python library
	//

	/* Because OpenSync likes to call this function multiple times in
	 * different threads, and because we may be sharing the python
	 * interpreter with other code, we have to:
	 *  * init python only once
	 *  * acquire the Python lock before making any API calls
	 */

	if (!Py_IsInitialized()) {
		/* We're the first user of python in this process. Initialise
                 * it, enable threading, and release the lock that will be
                 * re-acquired by the PyGILState_Ensure() call below. */
		Py_InitializeEx(0);
		PyEval_InitThreads();
		PyThreadState *pts = PyGILState_GetThisThreadState();
		PyEval_ReleaseThread(pts);
	} else if (!PyEval_ThreadsInitialized()) {
		/* Python has been initialised, but threads are not. */
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "The Python interpreter in this process has been initialised without threading support.");
		osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
		return FALSE;
	}

	PyGILState_STATE pystate = PyGILState_Ensure();

	if (!set_search_path(&error))
		goto state_error;

	// import opensync module
	PyObject *osync_module = pm_load_opensync(&error);
	if (!osync_module)
		goto state_error;

	PyGILState_Release(pystate);


	//
	// create plugin
	//
	plugin = osync_plugin_new(&error);
	if (!plugin)
		goto error;

	osync_plugin_set_initialize_func(plugin, pm_initialize);
	osync_plugin_set_finalize_func(plugin, pm_finalize);
	osync_plugin_set_discover_func(plugin, pm_discover);
	osync_plugin_set_data(plugin, g_strdup(modulename));


	//
	// create client
	//
	client = osync_client_new(&error);
	if (!client)
		goto error;

	osync_client_set_pipe_path(client, pipe_path);
	osync_client_set_plugin(client, plugin);
	osync_plugin_unref(plugin);


	printf("[python_module %s]: %s OSyncPlugin:%p OSyncClient:%p\n",
		modulename, __func__, plugin, client);
	printf("[python_module %s]: Starting (blocking) OSyncClient ...\n",
		modulename);

	if (!osync_client_run_and_block(client, &error))
		goto error;

	printf("[python_module %s]: OSyncClient completed.\n", modulename);

	osync_client_unref(client);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return 0;

state_error:
	PyGILState_Release(pystate);

error:
	fprintf(stderr, "[python_module %s] Error: %s\n",
		modulename, osync_error_print(&error));
	osync_error_unref(&error);
	osync_trace(TRACE_EXIT_ERROR, "%s", __func__);
	return 1;
}

