opensync = __import__('opensync1')
import binascii

class DummySink(opensync.ObjTypeSinkCallbacks):
	def __init__(self, info, objtype=None, objtypesink=None):
		opensync.ObjTypeSinkCallbacks.__init__(self, objtype, objtypesink)
		# Uncomment the following line for a NO_CONFIG plugin
		#self.sink.add_objformat_sink(opensync.ObjFormatSink("plain"))
		self.objformat = info.format_env.find_objformat("plain")
		# Comment the following lines for a NO_CONFIG plugin
		res = info.config.find_active_resource(self.sink.name)
		self.path = res.path
		print "Using path: ", self.path

	def connect(self, info, ctx):
		print "Connect called!"

	def get_changes(self, info, ctx, slow_sync):
		print "get_changes called!"
		if slow_sync:
			print "Slow-sync requested"
		change = opensync.Change()
		change.uid = "testuid"
		change.data = opensync.Data(
			"This is test data from python-sample", self.objformat)
		change.objtype = "data"
		change.changetype = opensync.CHANGE_TYPE_ADDED
		ctx.report_change(change)
		print "done with get_changeinfo"
	
	def commit(self, info, ctx, chg):
		print "commit called!"
		print "Opensync wants me to commit data for UID", chg.uid
		print "The data is: ", binascii.b2a_qp(chg.data.data)
		print "Printable: ", chg.data.get_printable()
	
	def committed_all(self, info, ctx):
		print "committed_all called!"

	def read(self, info, ctx, chg):
		print "read called!"
		print "OpenSync wants me to read the data for UID", chg.uid

	def disconnect(self, info, ctx):
		print "disconnect called!"

	def sync_done(self, info, ctx):
		print "sync_done called!"

	def connect_done(self, info, ctx, slow_sync):
		print "connect_done called!"

def initialize(info):
	print "initialize called!"

	# Comment the following lines if you want a NO_CONFIG plugin
	# since info.config does not exist in that case.
	print "My config is:", info.config
	print "Found sinks", info.num_objtype_sinks()
	for sink in info.objtypes:
		dummysink = DummySink(info, None, sink)

	# Uncomment the following for a NO_CONFIG plugin, which
	# must create it's own sinks
	#print "Adding new sink"
	#dummysink = DummySink("data", info)
	#print "Dummy sink: ", dummysink
	#info.add_objtype(dummysink.sink)

	print "Done"

	return

# the data parameter was returned by initialize, and is optional
def discover(info, data):
	print "discover called!"
	for sink in info.objtypes:
		print "setting sink available:", sink.name
		sink.available = True
	# Uncomment the following for a NO_CONFIG plugin
	# This creates the plugin config on the fly, adding a resource
	# using "plain" objformat and "data" objtype.
	#res = opensync.PluginResource()
	#res.add_objformat_sink(opensync.ObjFormatSink("plain"))
	#res.objtype = "data"
	#res.enabled = True
	#info.config = opensync.PluginConfig()
	#info.config.add_resource(res)
	# end of NO_CONFIG code
	info.version = opensync.Version()
	info.version.plugin = "python-sample"
	print "done"

# the data parameter was returned by initialize, and is optional
def finalize(data):
	print "finalize called!"
	print "Done"
	return

